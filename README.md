# iLCSoft
[![Build status](https://gitlab.cern.ch/CLICdp/SoftwareConfigurations/iLCSoft/badges/master/pipeline.svg)](https://gitlab.cern.ch/CLICdp/SoftwareConfigurations/iLCSoft/commits/master)

Repository housing configuration scripts for deploying iLCSoft automatically via Gitlab Continuous Integration.

When making a tag, you can modify which versions are beeing used for the build via the [tag massage](https://gitlab.cern.ch/CLICdp/SoftwareConfigurations/iLCSoft/tags/new) e.g.
```
[ilcinstall/builds/release-versions-HEAD.py]
LCFIPlus_version = "v00-06-09"
```
a list of version names can be found [here](https://github.com/iLCSoft/iLCInstall/blob/master/builds/release-versions-HEAD.py)

Via the same mechanism also modifications to the release cfg can be made (change a repo to another user) e.g.
```
[ilcinstall/builds/release-ilcsoft.cfg]
ilcsoft.module("LCIO").download.gitrepo="petricm"

[ilcinstall/builds/release-ilcsoft7.cfg]
ilcsoft.module("LCIO").download.gitrepo="petricm"
```
the file `release-ilcsoft.cfg` coresponds to the GCC6.2/LLVM3.9 build and `release-ilcsoft7.cfg` to the GCC7/LLVM5 build.

The file in this repo `mybuilds/release-ilcsoft-lcg.cfg` is based on some packages from LCG.

Pipelines of this project are automatically triggered by merges to the following repositories:

| Packages |
| -------- |
| [ConformalTracking](https://github.com/iLCSoft/ConformalTracking/) |
| [DD4hep](https://github.com/AIDASoft/DD4hep) |
| [DDMarlinPandora](https://github.com/iLCSoft/DDMarlinPandora/) |
| [lcgeo](https://github.com/iLCSoft/lcgeo/) |
| [LCIO](https://github.com/iLCSoft/LCIO/) |
| [Marlin](https://github.com/iLCSoft/Marlin/) |
| [MarlinReco](https://github.com/iLCSoft/MarlinReco/) |
| [MarlinTrk](https://github.com/iLCSoft/MarlinTrk/) |
| [MarlinTrkProcessors](https://github.com/iLCSoft/MarlinTrkProcessors/) |
| [Overlay](https://github.com/iLCSoft/Overlay/) |
