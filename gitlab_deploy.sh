#!/bin/bash

#Get the line for the CVMFS status and chech if server is transaction
clicdp_status=`cvmfs_server list | grep clicdp`
if [[ $clicdp_status == *"(stratum0 / S3)"* ]]; then
  echo "I am not in transaction"
  # Start transaction
  if ! cvmfs_server transaction clicdp.cern.ch; then
    echo "Opening transation failed, aborting"
    cvmfs_server abort -f clicdp.cern.ch
    exit 1
  fi
  # Deploy the nightly iLCSoft build
  if ! ${2}/install_gitlab_ilcsoft.sh $1 $2; then
    echo "Installing ILCSoft builds failed, aborting"
    cvmfs_server abort -f clicdp.cern.ch
    exit 1
  fi
  # Publish changes
  if ! cvmfs_server publish clicdp.cern.ch; then
    echo "Publication failed, aborting"
    cvmfs_server abort -f clicdp.cern.ch
    exit 1
  fi
  exit 0
else
  (>&2 echo "#################################")
  (>&2 echo "### CVMFS Transaction ongoing ###")
  (>&2 echo "### Nightly deploy cancelled  ###")
  (>&2 echo "#################################")
  exit 1
fi
