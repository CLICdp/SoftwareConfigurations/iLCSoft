#!/bin/bash

cd $2

#Deleting old nightly build if it is not a tag
if [ "$1" == "nightly" ]; then
  echo "Deleting old nightly build"
  rm -rf /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly
  rm -rf /cvmfs/clicdp.cern.ch/iLCSoft/lcg/99python2
  rm -rf /cvmfs/clicdp.cern.ch/iLCSoft/lcg/100/nightly
  rm -rf /cvmfs/clicdp.cern.ch/iLCSoft/lcg/104/nightly
  rm -rf /cvmfs/clicdp.cern.ch/iLCSoft/lcg/dev4/nightly
fi

# Move new build into place
echo "Moving new build into place"
cp -r cvmfs/clicdp.cern.ch/iLCSoft/builds/$1 /cvmfs/clicdp.cern.ch/iLCSoft/builds/
# single builds have to be done like this
cp -r cvmfs/clicdp.cern.ch/iLCSoft/lcg/dev4/$1/ /cvmfs/clicdp.cern.ch/iLCSoft/lcg/dev4/$1/
# multiple builds can be copied like this
cp -r cvmfs/clicdp.cern.ch/iLCSoft/lcg/100/$1 /cvmfs/clicdp.cern.ch/iLCSoft/lcg/100/
cp -r cvmfs/clicdp.cern.ch/iLCSoft/lcg/104/$1 /cvmfs/clicdp.cern.ch/iLCSoft/lcg/104/
