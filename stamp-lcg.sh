#!/bin/bash

export stamp=`date -u`

echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/lcg/${LCG_RELEASE}/${BUILD_PATH}/${BUILD_ARCH}/init_ilcsoft.sh
echo "echo \"Build created on ${stamp}\""                      >> /cvmfs/clicdp.cern.ch/iLCSoft/lcg/${LCG_RELEASE}/${BUILD_PATH}/${BUILD_ARCH}/init_ilcsoft.sh
echo "echo \"Based on LCG_${LCG_RELEASE}\""                    >> /cvmfs/clicdp.cern.ch/iLCSoft/lcg/${LCG_RELEASE}/${BUILD_PATH}/${BUILD_ARCH}/init_ilcsoft.sh
echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/lcg/${LCG_RELEASE}/${BUILD_PATH}/${BUILD_ARCH}/init_ilcsoft.sh
