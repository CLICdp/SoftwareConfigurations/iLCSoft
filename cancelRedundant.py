#!/usr/bin/env python3

import os
import json

import six.moves.urllib

url = "https://gitlab.cern.ch/api/v4/projects/7828/pipelines"
request = six.moves.urllib.request.Request(url, headers={"PRIVATE-TOKEN" : str(os.environ['API_TOKEN'])})
contents = six.moves.urllib.request.urlopen(request).read()
data = json.loads(contents)


for i in range(0, len(data)):
  if data[i]['status'] == 'running':
    if str(data[i]['id']) != str(os.environ['CI_PIPELINE_ID']):
      job_url = "https://gitlab.cern.ch/api/v4/projects/7828/pipelines/%s/jobs?scope[]=running" % data[i]['id']
      job_request = six.moves.urllib.request.Request(job_url, headers={"PRIVATE-TOKEN" : str(os.environ['API_TOKEN'])})
      job_contents = six.moves.urllib.request.urlopen(job_request).read()
      job_data = json.loads(job_contents)
      if len(job_data) >= 1 :
        if str(job_data[0]['stage']) != "deploy" and not job_data[0]['tag'] :
          os.system( "curl -g -X POST --header \"PRIVATE-TOKEN:$API_TOKEN\" \"https://gitlab.cern.ch/api/v4/projects/7828/pipelines/%s/cancel\"" % data[i]['id'] )
