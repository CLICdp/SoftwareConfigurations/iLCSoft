#!/bin/bash

export stamp=`date -u`

if [[ ${COMPILER_TYPE} == "gcc" ]]; then
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
    echo "echo \"Build created on ${stamp}\""                      >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
fi

if [[ ${COMPILER_TYPE} == "llvm" ]]; then
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
    echo "echo \"Build created on ${stamp}\""                      >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
    echo "echo \"----------------------------------------------\"" >> /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
fi

if [[ ${COMPILER_VERSION} == "llvm39" ]]; then
    sed -i '9s/.*/export\ LD_LIBRARY_PATH=\/cvmfs\/clicdp.cern.ch\/compilers\/llvm\/3.9.0\/x86_64-slc6\/lib64:\/cvmfs\/clicdp.cern.ch\/compilers\/llvm\/3.9.0\/x86_64-slc6\/lib:\/cvmfs\/clicdp.cern.ch\/software\/Python\/2.7.12\/x86_64-slc6-llvm39-opt\/lib:\/cvmfs\/clicdp.cern.ch\/compilers\/gcc\/6.2.0\/x86_64-slc6\/lib64:${LD_LIBRARY_PATH}/' /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
fi

if [[ ${COMPILER_VERSION} == "llvm5" ]]; then
sed -i '9s/.*/export\ LD_LIBRARY_PATH=\/cvmfs\/clicdp.cern.ch\/compilers\/llvm\/5.0.1\/x86_64-slc6\/lib64:\/cvmfs\/clicdp.cern.ch\/compilers\/llvm\/5.0.1\/x86_64-slc6\/lib:\/cvmfs\/clicdp.cern.ch\/software\/Python\/2.7.14\/x86_64-slc6-llvm5-opt\/lib:\/cvmfs\/clicdp.cern.ch\/compilers\/gcc\/7.3.0\/x86_64-slc6\/lib64:${LD_LIBRARY_PATH}/' /cvmfs/clicdp.cern.ch/iLCSoft/builds/${BUILD_PATH}/x86_64-slc6-${COMPILER_VERSION}-opt/init_ilcsoft.sh
fi
