#!/usr/bin/env python3

import io
import os
import sys
import json

import six.moves.urllib
from  six.moves.configparser import RawConfigParser


if os.environ.get('CI_COMMIT_TAG') is None:
  print("Not a tag, you cannot modify this build!")
  exit(0)

tagName=str(os.environ['CI_COMMIT_TAG'])

#Read commit msg via api
url = "https://gitlab.cern.ch/api/v4/projects/7828/repository/tags/"+tagName
request = six.moves.urllib.request.Request(url, headers={"PRIVATE-TOKEN" : str(os.environ['API_TOKEN'])})
contents = six.moves.urllib.request.urlopen(request).read()
data = json.loads(contents)

#check if msg present
if not data["message"]:
  print("No message in tag -> nothing to do.")
  exit(0)

commitMsg=str(data["message"])
print(commitMsg)

config = ConfigParser.RawConfigParser(allow_no_value=True)
# do not make all strings lowercase
config.optionxform = str

#check if msg can be parsed
try:
  config.readfp(io.BytesIO(commitMsg))
except ConfigParser.MissingSectionHeaderError:
  print("No configuration found in CI_COMMIT_MESSAGE")
  exit(0)


for section in config.sections():
  if not os.path.isfile(section):
    print("File ", section, " does not exist!")
    exit(1)
  options = config.options(section)
  for option in options:
    with open(section, "a") as confFile:
      confFile.write(option + " = " + config.get(section, option) + "\n")
