#!/bin/env python
"""Get the release notes between given dates for all packages."""

import argparse
from datetime import datetime, timedelta
import logging
from collections import defaultdict
from pprint import pformat
import textwrap

import requests

SUPER_DEBUG = True

logging.basicConfig(level=logging.INFO, format='%(name)-30s %(levelname)-5s: %(message)s')
LOG = logging.getLogger('')

try:
  from GitTokens import GITHUBTOKEN
except ImportError:
  raise ImportError("""Failed to import GITHUBTOKEN please point the pythonpath to your GitTokens.py file which contains
                    your "Personal Access Token" for Github

                    I.e.:
                    Filename: GitTokens.py
                    Content:
                    ```
                    GITHUBTOKEN = "e0b83063396fc632646603f113437de9"
                    ```
                    (without the triple quotes)
                    """
                    )

SESSION = requests.Session()
SESSION.headers.update({'Authorization': "token %s " % GITHUBTOKEN})

def req2Json(url, parameterDict, requestType):
  """ call to github api using requests package if available """
  log = LOG.getChild("GitHub")
  log.debug("Running %s with %s ", requestType, parameterDict)
  req = getattr(SESSION, requestType.lower())(url, json=parameterDict)
  if req.status_code not in (200, 201):
    log.error("Unable to access API: %s", req.text)
    raise RuntimeError("Failed to access API")
  parsed = req.json()
  log.debug("Result obtained: %s", pformat(parsed)) if SUPER_DEBUG else ''  # pylint: disable=W0106
  return parsed


def checkRate():
  """ return the result for check_rate call """
  rate = req2Json(url="https://api.github.com/rate_limit", parameterDict={}, requestType='get') 
  LOG.getChild("Rate").info("Remaining calls to github API are %(remaining)s of %(limit)s",
                             rate['rate'])


def parseForReleaseNotes(commentBody):
  """Will look for "BEGINRELEASENOTES / ENDRELEAENOTES" and extend releaseNoteList if there are entries."""
  relNotes = []
  if not all(tag in commentBody for tag in ("BEGINRELEASENOTES", "ENDRELEASENOTES")):
    return relNotes
  releaseNotes = commentBody.split("BEGINRELEASENOTES")[1].split("ENDRELEASENOTES")[0]
  for line in releaseNotes.splitlines():
    if line.strip():
      relNotes.append(line)
  return relNotes


def _parsePrintLevel(level):
  """Translate printlevel to logging level."""
  LOG.setLevel(dict(CRITICAL=logging.CRITICAL,
                    ERROR=logging.ERROR,
                    WARNING=logging.WARNING,
                    INFO=logging.INFO,
                    DEBUG=logging.DEBUG,
                  )[level])


class GetReleaseNotes(object):
  """Get all release notes for given packages between two dates and format them."""

  def __init__(self):
    self.startDate = str(datetime.now() - timedelta(days=14))[:10]
    self.endDate = str(datetime.now())[:10]
    self.packages = []

    self.printLevel = 'INFO'
    self.configFile = None
    self.errors = []
    self._releaseNotes = defaultdict(list)

  def _parseConfigFile( self ):
    """Parses the config file and files packages list."""
    log = LOG.getChild('ParseConfig')
    log.debug('Checking for configFile...')
    if self.configFile is None:
      log.debug('...no configfile')
      return
    with open(self.configFile) as conf:
      log.debug('...opening config file %r', self.configFile)
      for line in conf.readlines():
        if not line.strip():
          continue
        if line.strip()[0]=="#":
          continue
        parts = line.strip().split(' ')
        if len(parts) != 1:
          self.errors.append("Line '%s' cannot be parsed" % line.strip())
        else:
          self.packages.append(line.strip())
    return

  def parsePackage(self, packageString):
    """Return tuple of owner, repo, branch.

    default owner: ilcsoft
    default branch: master
    """
    slashCount = packageString.count('/')
    if slashCount == 0:
      owner = "iLCSoft"
      package = packageString
      branch='master'
    elif slashCount == 1:
      owner = packageString.split('/')[0]
      package = packageString.split('/')[1]
      branch='master'
    elif slashCount == 2:
      owner = packageString.split('/')[0]
      package = packageString.split('/')[1]
      branch='master'
    else:
      LOG.getChild('parsePackage').error('Failed to parase this package %r', packageString)
      raise RuntimeError('BadPackage')

    LOG.getChild('parsePackage').debug("Added package: %s %s %s", owner, package, branch)
    return owner, package, branch

  def parseOptions(self):
    """Parse the command line options."""
    parser = argparse.ArgumentParser("Making ILCSoft Tags",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-v", "--printLevel", action="store", default=self.printLevel, dest="printLevel",
                        choices=('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'),
                        help="Verbosity DEBUG, INFO, WARNING, ERROR, CRITICAL")
    parser.add_argument("--checkRate", action="store_true", dest="checkRate", default=False,
                        help="Print out remaining number of queries for this hour")

    parser.add_argument("--packages", action="store", default=self.packages, nargs='+',
                        help="packages to check: [owner/]package[/version[/branch]]")

    parser.add_argument("--startDate", action="store", dest="startDate", default=self.startDate,
                        help="At what date 'YYYY-MM-DD' to start getting PRs")

    parser.add_argument("--endDate", action="store", dest="endDate", default=self.endDate,
                        help="At what date 'YYYY-MM-DD' to stop getting PRs")

    parser.add_argument("--file", action="store", default=self.configFile, dest="configFile",
                        help="file holding packages to tag")

    parsed = parser.parse_args()
    self.packages = parsed.packages
    self.configFile = parsed.configFile
    self.startDate = parsed.startDate
    self.endDate = parsed.endDate
    _parsePrintLevel(parsed.printLevel)
    self._parseConfigFile()
    self.startDate = parsed.startDate
    self.endDate = parsed.endDate

    if parsed.checkRate:
      checkRate()

  def run( self ):
    """Execute everything."""
    for package in self.packages:
      owner, package, branch= self.parsePackage(package)
      try:
        self.getReleaseNotes(owner, package, branch)
      except RuntimeError as e:
        LOG.error("Failure for %s/%s/%s", owner, package, branch)
        self.errors.append(str(e))

    self.printReleaseNotes()

    checkRate()

  def printReleaseNotes(self):
    """Format the release notes into a string."""
    relNotesString = '# Release of %s' % self.endDate
    log = LOG.getChild('print')
    for package, prs in sorted(self._releaseNotes.items()):
      relNotesString += '\n\n## %s\n' % package
      logg = log.getChild(package)
      for prID, info in prs.items():
        info['id'] = prID
        logg.debug('%(id)s -- %(date)s: %(notes)s', info)
        prString = '[(#%s)](%s)' % (prID, info['url'])
        for line in info['notes'].splitlines():
          logg.info('checking line %r', line)
          if line.strip().startswith('-'):
            line = line.replace('-', '- %s ' % prString, 1)
          if line.strip().startswith('*'):
            line = line.replace('*', '- %s ' % prString, 1)
          relNotesString += '\n  ' + line
    while '\n\n\n' in relNotesString:
      relNotesString = relNotesString.replace('\n\n\n', '\n\n')
    print(relNotesString)

  def getReleaseNotes(self, owner, package, branch):
    """Parses the PRs for ReleaseNotes to collate them."""
    log = LOG.getChild('getRel.%s' % package)
    prs = self._getPRsSinceLatestTag(owner, package, branch)
    if not prs:
      log.info('No PRs found for %s/%s/%s', owner, package, branch)
      return

    relNotes = defaultdict(lambda: defaultdict(dict))
    log.info('Getting release notes from %d PR', len(prs))
    for aPR in prs:
      prID = aPR['number']
      notes = parseForReleaseNotes(aPR['body'])
      if notes:
        relNotes[package][prID]['notes'] = '\n'.join(notes)
        relNotes[package][prID]['date'] = aPR['merged_at'][:10]
        relNotes[package][prID]['url'] = aPR['html_url']
    self._releaseNotes.update(relNotes)
    log.info("... finished getting release notes")
    return

  def _getPRsSinceLatestTag(self, owner, package, branch):
    """Return the PRs since the startDate, before enddate for given branch."""
    log = LOG.getChild('getPRs.%s' % package)
    mergedPRs = self._getGithubPRs(owner=owner, package=package, state="closed", mergedOnly=True)
    prs = []
    for aPR in mergedPRs:
      if aPR['merged_at'] > self.startDate and aPR['base']['label'].endswith(branch) and \
         aPR['merged_at'] <= self.endDate:
        log.debug('PR %r was merged inside date range', aPR['number'])
        prs.append(aPR)
      else:
        log.debug('PR %s was merged _OUTSIDE_ date range or branch', aPR['number'])
    if prs:
      log.info('PRs merged inside date range: %s',
               ', '.join(sorted(str(aPR['number']) for aPR in prs)))
    return prs

  def _getGithubPRs(self, owner, package, state="open", mergedOnly=False, perPage=100):
    """Get all PullRequests from github

    :param str state: state of the PRs, open/closed/all, default open
    :param bool merged: if PR has to be merged, only sensible for state=closed
    :returns: list of githubPRs
    """
    url = self._github('pulls?state=%s&per_page=%s' % (state, perPage), owner, package)
    prs = req2Json(url=url, parameterDict={}, requestType='get')
    if not mergedOnly:
      return prs
    # only merged PRs
    prsToReturn = []
    for pr in prs:
      if pr.get('merged_at', None) is not None:
        prsToReturn.append(pr)
    return prsToReturn

  @staticmethod
  def _github(action, owner, package):
    """Return the url to perform actions on github.

    :param str action: command to use in the gitlab API, see documentation there
    :returns: url to be used by curl
    """
    options = dict(owner=owner, repo=package, action=action)
    ghURL = "https://api.github.com/repos/%(owner)s/%(repo)s/%(action)s" % options
    return ghURL


if __name__ == "__main__":
  RUNNER = GetReleaseNotes()
  try:
    RUNNER.parseOptions()
  except RuntimeError as e:
    LOG.error("Error during runtime: %s", e)
    exit(1)

  try:
    RUNNER.run()
  except RuntimeError as e:
    LOG.error("Error during runtime: %s", e)
    exit(1)
