#!/usr/bin/env python3

import tarfile
import sys
from pprint import pprint


archTuples = [arch.split('-', 1) for arch in sys.argv[1:]]
files = ['ilcsoft_lcg_%s-%s.tar' % (lcg, arch) for (lcg, arch) in archTuples]

pprint(archTuples)
pprint(files)

for file in files:
  if not tarfile.is_tarfile(file):
    sys.exit(1)
