#!/usr/bin/env python3

import sys

initFile = sys.argv[1]

theFix = """

# sort LD library path to have clicdp come before sft
export LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ":" "\n" | sort -u | xargs | tr " " ":"`

# set ROOT_INCLUDE_PATH to the include of the view, because Vc.pcm is causing issues otherwise
export ROOT_INCLUDE_PATH=${ROOT_INCLUDE_PATH}:$ROOTSYS/include

"""

with open(initFile, "at") as theFile:
  theFile.write(theFix)
