#!/usr/bin/env python3

import os
import json
import sys
from pprint import pprint

archTuples = [arch.split('-', 1) for arch in sys.argv[1:]]

TAR_BALL_DICT = {
  'build_arches: [%s, %s' % (arch, lcg) : 'ilcsoft_lcg_%s-%s.tar' % (lcg, arch) for (lcg, arch) in archTuples
}

pprint(TAR_BALL_DICT)

with open('pipeline_info.json') as json_data:
  data = json.load(json_data)

pprint(data)

for i, dataLine in enumerate(data):
  for buildName, tarballName in TAR_BALL_DICT.items():
    if buildName in dataLine['name']:
      print("Downloading %s build %s" % (dataLine['name'], tarballName))
      os.system("curl -O https://gitlab.cern.ch/CLICdp/SoftwareConfigurations/iLCSoft/-/jobs/%s/artifacts/raw/%s" %
                (dataLine['id'], tarballName))
